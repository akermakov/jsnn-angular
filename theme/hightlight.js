var Highlight = require('highlight')
var html = require('highlight-xml');
var js = require('highlight-javascript');

var highlight = new Highlight()
  .use(html)
  .use(js);

var el = document.querySelectorAll('code');
highlight.all();